#!/usr/bin/python
# -*- coding: utf-8 -*-
import ephem
from datetime import datetime, date, time
import argparse


class DatetimeOfAzimuth:
    """
    Provides the datetime from the position of the sun.
    """
    def __init__(self, latitude = 49.0 + 37.0/60 + 18.0/3600, longitude = 5.0 + 43.0 / 60 + 9.0 / 3600):
        # Default lat/long are from Meix-le-Tige
        self.mlt_observer = ephem.Observer()
        self.mlt_observer.lat = str(latitude)
        self.mlt_observer.lon = str(longitude)
        # Start the calculation from about mid of the day.
        self.sun = ephem.Sun()

        # Make a first computation (not used)
        self.sun.compute(self.mlt_observer)

    def f(self, x):
        self.mlt_observer.date = x
        self.sun.compute(self.mlt_observer)
        return self.sun.az - ephem.degrees(str(self.azimuth))

    def get_localtime_from_azimuth(self, azimuth):
        self.azimuth = str(azimuth)
        self.mlt_observer.date = datetime.combine(date.today(), time(14))

        x = self.mlt_observer.date
        ephem.newton(self.f, x, x + 0.02)
        return ephem.localtime(self.mlt_observer.date)

    def get_utctime_from_azimuth(self, azimuth):
        self.azimuth = str(azimuth)
        self.mlt_observer.date = datetime.combine(date.today(), time(14))

        x = self.mlt_observer.date
        ephem.newton(self.f, x, x + 0.02)
        return self.mlt_observer.date.datetime()
    
    def get_azimuth_from_utctime(self, localtime):
        self.mlt_observer.date = localtime
        self.sun.compute(self.mlt_observer)
        return self.sun.az


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--sunaz", help="Provide Azimuth of the Sun you would like to have the time for (today)", required=True)
    args = parser.parse_args()
    doa = DatetimeOfAzimuth()
    # Volet SAM: Fermeture @17:35 le 4/8/2018
    # Soleil perpendiculaire à la fenêtre de porte: @19:30 le 4/8/2018
    print("Today, when the sun is at %s ° asimuth, it's %s local time" % (args.sunaz, doa.get_localtime_from_azimuth(args.sunaz)))
