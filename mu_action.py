#!/usr/bin/python2

# Echo client program
import socket
import sys

#print str(sys.argv), str(len(sys.argv))

if len(sys.argv)<2:
    sys.exit()

HOST = '127.0.0.1'    # The remote host
PORT = 11111              # The same port as used by the server
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))


commandes = ['EXIT', 'ALARM', 'UPTIME', 'STATUS', 'STATUS_VEXO', 'RELOAD', 'RELOAD_CONFIG', 'RELOAD_BUTTONS', 'RELOAD_SENSORS', 'RELOAD_ACTIONS', 'STORES', 'MONITORED_EVENT_LIST', 'MONITORED_EVENT_CLEAR', 'MONITORED_EVENT_ALL', 'RELOAD_SENSOR_DURATION', 'RELOAD_CONDITIONS', 'RELOAD_CONDITIONS_ORDER', 'RELOAD_CONDITIONS_ARG', 'FORCE_UPDATE', ]

a = []
if   sys.argv[1] in commandes:
    if len(sys.argv)>2:
        s.send(sys.argv[1]+' '+sys.argv[2])
    else:
        s.send(sys.argv[1])
    data = s.recv(4096)
    print 'Response: ', repr(data)
elif sys.argv[1]=='--source':
    i=3
    while i+3 <= len(sys.argv):
        a.append(sys.argv[i]+' '+sys.argv[i+1]+' '+sys.argv[i+2]+' '+sys.argv[2])
        i += 3
else:
    i=1
    while i+3 <= len(sys.argv):
        a.append(sys.argv[i]+' '+sys.argv[i+1]+' '+sys.argv[i+2])
        i += 3


for action in a:
        s.send('ACTION '+action+' MUsocket')
        data = s.recv(4096)

#print 'Received', repr(data)

s.close()

