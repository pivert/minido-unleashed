#!/bin/bash

# Auteur     : MATHIAS Francois
# Date       : 20/11/2010
# Version    : 0.9
# Description: Insert dans la DB les valeurs (des triplets) passées en argument
#              Réessaye tant que la valeur de retour est différente de 0 (Ex: 5 lsq DB lockée)
#              Insére dans un fichier de log la date, la valeur de retour et la requete.
#              Si 1er argument == sonnette alors fait sonner le carillon.

if [ "$1" = "--source" ]; then
  SOURCE="'$2'"
  shift 2
else
  SOURCE="NULL"
fi

if [ "$1" = "sonnette" ]; then

  A="     INSERT INTO exo_scheduler VALUES ('49', '5', datetime('now', 'localtime'), NULL, 'ON', $SOURCE); "
  A="${A} INSERT INTO exo_scheduler VALUES ('49', '5', datetime('now', 'localtime', '+0.5 seconds'), NULL, 'OFF', $SOURCE);"

else

  A=""
  while [ $# -ge 3 ]; do
    A="${A} INSERT INTO exo_scheduler VALUES ('$1', '$2', datetime('now', 'localtime'), NULL, '$3', $SOURCE);"
    shift 3
  done

fi

ok=1
while [ $ok -ne 0 ]; do
    sqlite3 /opt/minido-unleashed/db/minidodb "$A"
    ok=$?
    echo "`date '+%Y-%m-%d %H:%M:%S'` $ok $A" >> /opt/minido-unleashed/log/mu.log
done
