#!/bin/bash
/usr/bin/sqlite3 /opt/minido-unleashed/db/minidodb "delete from exo_scheduler where s_time < datetime('now', '-3600 seconds','localtime');" 
/bin/sleep 2
/usr/bin/sqlite3 /opt/minido-unleashed/db/minidodb "delete from minido_log where log_time < datetime('now', '-31 days','localtime');" 
