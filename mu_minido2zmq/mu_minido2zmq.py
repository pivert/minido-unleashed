#!/usr/bin/python
import sys
import msgpack
import json
import datetime
from twisted.python import usage, log
from twisted.internet.protocol import Protocol
from twisted.internet import protocol

def checksum(datalist):
    """ Calculate an XOR checksum """
    checksum_ = 0
    for item in datalist:
        checksum_ ^= item
    return checksum_

class MuProtocol(Protocol):
    def __init__(self, zmqpubconn, zmqpubjsonconn):
        self.chardata = list()
        self.zmqpubconn = zmqpubconn
        self.zmqpubjsonconn = zmqpubjsonconn

    def dataReceived(self, chardata):
	dstr = '|'.join([format(ord(x), '02X') for x in chardata])
        print("Debug: Received new data on bus: {}".format(dstr))
        self.chardata.extend(list(chardata))
        # The smallest packets are 6 bytes long. So even if the data length is
        # in 4th position, we need at least 6 bytes with the checksum.
        while len(self.chardata) >= 6:
            if ord(self.chardata[0]) != 0x23:
                startidx = 0
                try:
                    startidx = self.chardata.index(chr(0x23))
                except(LookupError, ValueError):
                    # We did not find 0x23
                    # We are not interested in data not starting by 0x23.
                    # print(str(datetime.datetime.now()) +
                        # " Error : none is 0x23. Dropping everything.")
                    # print('|'.join([format(ord(x), '02X') for x in self.chardata]))
                    dstr = '|'.join([format(ord(x), '02X') for x in self.chardata])
                    print("Error : none is 0x23. Dropping : {}".format(dstr, ))
                    self.chardata = list()
                    continue

                if startidx != 0:
                    print('Deleting first characters : StartIDX={} Deleted characters={} '.format(startidx, str(self.chardata[:startidx])))
                    self.chardata = self.chardata[startidx:]
                    continue
            else:
                """ We have a valid begining """
                datalength = ord(self.chardata[3])
                if len(self.chardata) >= datalength + 4:
                    """ We have at least a complete packet"""
                    if checksum(
                        [ord(x) for x in self.chardata[4:datalength + 3]]
                        ) != ord(self.chardata[datalength + 3]):
                        print("Warning : Invalid checksum for packet : "
                            + '|'.join([format(ord(x), '02X') for x in self.chardata]))
                        validpacket = self.chardata[0:datalength + 4]
                        # +4 since it's the headers
                        # +1 for the checksum
                        self.chardata = self.chardata[datalength + 4 + 1:]
                        # print("Remaining characters in the queue: {}".format('|'.join([format(ord(x), '02X') for x in self.chardata])))
                        continue
                    else:
                        validpacket = self.chardata[0:datalength + 4]
                        # +1 for the checksum
                        self.chardata = self.chardata[datalength + 5:]
                    # OK, I have now a nice, complete, beautiful, valid packet
                    data = [ord(x) for x in validpacket]
                    self.newpacket(data)
                else:
                    print("Debug packet : " 
                        + '|'.join([format(ord(x), '02X') for x in self.chardata]))
                    break

    def newpacket(self, data):
        # Fixme: We should probably remove this fuction
        data_to_display = '|'.join([format(x, '02X') for x in data])
        print('From serial: %s to ZmqPub in MSGPACK & JSON' % (data_to_display, ))
        msgpack_data = msgpack.packb(data)
        self.zmqpubconn.publish(msgpack_data)
        # Strip the first and last character, as it's always the 35, and the checksum.
        self.zmqpubjsonconn.publish(json.dumps(data[1:-1]))

    def sendToSerial(self, data, tag=''):
        #msg = list(msgpack.unpackb(data))
        #Fixme: I probably must loop instead of taking the first element of the list
        msg = list(msgpack.unpackb(data[0]))
        if msg[len(msg)-1] == checksum( msg[4:(len(msg)-1)]):
            packet = ''.join([chr(x) for x in msg])
            self.transport.write(packet)
        else:
            # First check if the packet is complete, 
            # and build the missing checksum.
            if len(msg) == msg[3] + 3:
                print(str(datetime.datetime.now()) + " :",
                    "Adding the missing checksum")
                msg.append(checksum ( msg[4:(len(msg))] ))
                packet = ''.join([chr(x) for x in msg])
                self.transport.write(packet)

            else:
                print(str(datetime.datetime.now()) + " : " +
                    'Bad checksum / Cannot add checksum for packet : ' + str('|'.join(
                    [ '%0.2x' % c for c in msg ])))

        print('Send to Serial: %s' %(msg, ))
        # self.transport.write(''.join([chr(x) for x in msg]))
        # Publish back to other subscribers to propagate.
        print('From ZMQ: %s back to ZmqPub in MSGPACK & JSON' % ('|'.join([format(x, '02X') for x in msg]), ))
        self.zmqpubconn.publish(data[0])
        self.zmqpubjsonconn.publish(json.dumps(msg[1:-1]))

    def sendToSerialJson(self, data):
        msg = list(json.loads(data[0]))
        print('Received packet on the sendToSerialJson port:')
        print(msg)
        if msg[len(msg)-1] == checksum( msg[4:(len(msg)-1)]):
            packet = ''.join([chr(x) for x in msg])
            self.transport.write(packet)
        else:
            # First check if the packet is complete, 
            # and build the missing checksum.
            if len(msg) == msg[3] + 3:
                print(str(datetime.datetime.now()) + " :",
                    "Adding the missing checksum")
                msg.append(checksum ( msg[4:(len(msg))] ))
                packet = ''.join([chr(x) for x in msg])
                print('Send to Serial: %s' %('|'.join([format(x, '02X') for x in msg]), ))
                self.transport.write(packet)

            else:
                print(str(datetime.datetime.now()) + " : " +
                    'Bad checksum for packet : ' + str(' '.join(
                    [ '%0.2x' % c for c in msg ])))

        # print('Send to Serial: %s' %(msg, ))
        # self.transport.write(''.join([chr(x) for x in msg]))
        # Publish back to other subscribers to propagate.
        print('From ZMQ: %s back to ZmqPub in MSGPACK & JSON' % ('|'.join([format(x, '02X') for x in msg]), ))
        # self.zmqpubconn.publish(data[0])
        self.zmqpubconn.publish(msgpack.packb(msg))
        self.zmqpubjsonconn.publish(json.dumps(msg[1:-1]))

class Options(usage.Options):
    optParameters = [
            ['port', 'p', None, 'Device for RS-485 access to Minido or Alarm'],
            ['outfile', 'o', None, 'Logfile [default: sys.stdout]'],
            ]


# In case it's run from the command line
if __name__ == '__main__':
    from twisted.internet import reactor
    from twisted.internet.serialport import SerialPort
    from txzmq import ZmqFactory, ZmqEndpoint, ZmqPubConnection, ZmqPullConnection

    o = Options()
    try:
        o.parseOptions()
    except usage.UsageError, errortext:
        print("%s: %s" % (sys.argv[0], errortext))
        print("%s: Try --help for usage details." % (sys.argv[0]))
        raise SystemExit, 1

    if o.opts['port'] == None:
        print("You must provide a port to connect to ( check options with --help )")
        raise SystemExit, 1

    logFile = sys.stdout
    if o.opts['outfile']:
        logFile = o.opts['outfile']
    log.startLogging(logFile)

    zf = ZmqFactory()

    pube1 = ZmqEndpoint('bind', 'tcp://*:5557')
    zmqpubconn = ZmqPubConnection(zf, pube1)
    pubjson  = ZmqEndpoint('bind', 'tcp://*:5559')
    zmqpubjsonconn = ZmqPubConnection(zf, pubjson)

    pull1 = ZmqEndpoint('bind', 'tcp://*:5558')
    zmqpullconn = ZmqPullConnection(zf, pull1)

    pulljson = ZmqEndpoint('bind', 'tcp://*:5560')
    zmqpulljson = ZmqPullConnection(zf, pulljson)

    muprot = MuProtocol(zmqpubconn, zmqpubjsonconn)

    # Register call back
    zmqpullconn.onPull = muprot.sendToSerial
    zmqpulljson.onPull = muprot.sendToSerialJson

    serialport = SerialPort(muprot, o.opts['port'], reactor, baudrate=19200)
    reactor.run()

# In case it's a TAC application

