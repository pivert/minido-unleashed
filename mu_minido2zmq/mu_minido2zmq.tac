#!/usr/bin/python
from twisted.internet import reactor
from twisted.application import service
from twisted.application.service import Application

from twisted.internet.serialport import SerialPort
from txzmq import ZmqFactory, ZmqEndpoint, ZmqPubConnection, ZmqSubConnection, ZmqPullConnection

from mu_minido2zmq import *

class PubService(service.Service):
    def __init__(self):
        self.zf = ZmqFactory()

    def startService(self):
        pube1 = ZmqEndpoint('bind', 'tcp://*:5557')
        self.zmqpubconn = ZmqPubConnection(self.zf, pube1)

        pull1 = ZmqEndpoint('bind', 'tcp://*:5558')
        self.zmqpullconn = ZmqPullConnection(self.zf, pull1)

        pubjson  = ZmqEndpoint('bind', 'tcp://*:5559')
        self.zmqpubjsonconn = ZmqPubConnection(self.zf, pubjson)
    
        pulljson = ZmqEndpoint('bind', 'tcp://*:5560')
        self.zmqpulljson = ZmqPullConnection(self.zf, pulljson)

        muprot = MuProtocol(self.zmqpubconn, self.zmqpubjsonconn)
        self.zmqpullconn.onPull = muprot.sendToSerial
        self.zmqpulljson.onPull = muprot.sendToSerialJson
        #serialport = SerialPort(muprot, '/dev/serial/by-path/platform-bcm2708_usb-usb-0:1.3.1:1.0-port0', reactor, baudrate=19200)
        #serialport = SerialPort(muprot, '/dev/serial/by-path/platform-20980000.usb-usb-0:1.3.1:1.0-port0', reactor, baudrate=19200)
        serialport = SerialPort(muprot, '/dev/ttyUSB0', reactor, baudrate=19200)

    def stopService(self):
        del self.zmqpubconn
        del self.zmqpullconn
        del self.zmqpubjsonconn
        del self.zmqpulljson


application = Application('PUB and PULL Connections')
service = PubService()
service.setServiceParent(application)
