#!/usr/bin/python2
# EXI Range: 0x14-0x23 or 20-25 in base 10
# EXO Range: 0x3c-0x4b or 60-75 in base 10

import sys
import time
import datetime
import sqlite3
import re
import subprocess
import shlex
import copy
import socket
import serial
import os

sqlitedb = "/opt/minido-unleashed/db/minidodb"

MIN_DB_VERSION = 1.3  # -> version 1.3 obligatoire pour la DB

UPTIME = time.time()

DEBUG = 0

# Fonctions gerant les listes circulaires

def circularList_create(name,size):
    global circularLists
    if not name in circularLists:
        circularLists[name] = [size, -1, 1, ['' for i in range(0,size)] ] 
        
def circularList_add(name,value):
    global circularLists
    if name in circularLists:
        position = (circularLists[name][1] + 1) % circularLists[name][0]
        circularLists[name][1] = position
        circularLists[name][3][position] = value 
        
def circularList_view(name):
    if name in circularLists:
        for i in range(circularLists[name][2],circularLists[name][1]+circularLists[name][0]):
            position = i % circularLists[name][0]
        if circularLists[name][2]>circularLists[name][1]:
            Liste = circularLists[name][3][circularLists[name][2]+1:circularLists[name][0]]
            Liste.extend(circularLists[name][3][0:circularLists[name][1]+1])
            return Liste
        else:
            return circularLists[name][3][circularLists[name][2]+1:circularLists[name][1]+1]
    return []

def circularList_view2(name):
    global circularLists
    if name in circularLists:
        circularLists[name][2] = circularLists[name][1]
        Liste = circularList_view(name)
        return Liste
    return []
    
def circularList_view_all(name):
    global circularLists
    if name in circularLists:
        return circularLists[name][3]
    return []


# Autres fonctions

def add_Action_ActionsList(key,action):
    global ActionsList
    if key in ActionsList: ActionsList[key].append(action)
    else:                  ActionsList[key] = [action,]
    return

def key_lstrip_zero(type,major,minor):
    if str(minor) == '0': return type+'-'+major+'-0'
    else:                 return type+'-'+major+'-'+str(minor).lstrip('0')

def init_config():
    global config
    cur = conn.cursor()
    config = {}
    cur.execute("SELECT name, value FROM mu_config ")
    for row in cur.fetchall():
        config[str(row[0])] = str(row[1])
    if not 'Sensor_Duration'   in config: config['Sensor_Duration']   = '10.0' # securite
    if not 'Store_Duration'    in config: config['Store_Duration']    = '41.0' # securite
    if not 'BUTTON_LONG_PRESS' in config: config['BUTTON_LONG_PRESS'] = '2'    # securite

def init_buttons():
    global buttonsList
    cur = conn.cursor()
    buttonsList = {}
    cur.execute("SELECT exi_addr, button_addr, description FROM minido_buttons ORDER BY exi_addr, button_addr")
    for row in cur.fetchall():
        buttonsList[str(row[0]+"-"+str(row[1]))] = [str(row[2]), 0.0, ]

def init_sensors():
    global sensorsList
    cur = conn.cursor()
    sensorsList = {}
    cur.execute("SELECT byte, bit, type, name, location, floor FROM alarm_devices ORDER BY byte, bit")
    for row in cur.fetchall():
        sensorsList[str(row[0])+"-"+str(row[1])] = [row[0], row[1], str(row[2]), str(row[3]), 0, str(row[4]), str(row[5]), time.time(), ]

def init_actions():
    # Si maj actions alors: init_actions() + init_security_actions_conditions()
    global ActionsList
    cur = conn.cursor()
    ActionsList = {}
    cur.execute("SELECT src_module_type, src_addr, src_idx, src_command, dst_module_addr, dst_idx, delay, dst_command, duration FROM actions WHERE active='Yes' ")
    for row in cur.fetchall():
        if   str(row[0])=='EXI':       key = key_lstrip_zero('I',row[1],row[2])
        elif str(row[0])=='EXIL':      key = key_lstrip_zero('L',row[1],row[2])
        elif str(row[0])=='EXO':       key = 'O-'+row[1]+"-"+str(row[2])+"-"+str(row[3]).lower()
        elif str(row[0])=='EXOV':      key = 'V-'+row[1]+"-"+str(row[2])+"-"+str(row[3]).lower()
        elif str(row[0])=='Alarm':     key = 'A-'+row[1]+"-"+str(row[2])+"-"+str(row[3]).lower()
        elif str(row[0])=='Condition': key = 'C-'+str(row[2])+"-"+str(row[3]).lower()
        else:                          key = '?-'+row[1]+"-"+str(row[2])+"-"+str(row[3]).lower()
        action = [str(row[1]),str(row[2]),str(row[3]),str(row[4]),str(row[5]),str(row[6]),str(row[7]),str(row[8]), ]
        add_Action_ActionsList(key,action)

def init_devices():
    global devicesList, radiatorsList, storesList
    cur = conn.cursor()
    devicesList = {}
    cur.execute("SELECT module_addr, idx, type, location, name, description FROM minido_devices ORDER BY module_addr, idx")
    for row in cur.fetchall():
        devicesList[str(row[0]+"-"+str(row[1]))] = [str(row[0]),str(row[1]),str(row[2]),str(row[3]),str(row[4]),str(row[5]),]
    
    radiatorsList = {}
    for key in devicesList:
        if devicesList[key][2]=='Chauffage' and devicesList[key][4]=='Radiateur':
            radiatorsList[key] = [devicesList[key][0],devicesList[key][1],]

    storesList = {}
    for key in devicesList:
        if devicesList[key][2][0:5]=='Store':
            key2 = devicesList[key][3]+"#"+devicesList[key][4]
            if not key2 in storesList: storesList[key2] = ["", "", "",]  # [ key_up, key_down, last_action ('up' ou 'down')]
            if devicesList[key][2][5]=='U':
                storesList[key2][0] = key  # up
            else:
                storesList[key2][1] = key  # down
    
    # Initialisation storesList concernant la derniere action: up/down
    for key in storesList:
        addr_up   = devicesList[storesList[key][0]][0]
        idx_up    = devicesList[storesList[key][0]][1]
        addr_down = devicesList[storesList[key][1]][0]
        idx_down  = devicesList[storesList[key][1]][1]
        cur.execute("SELECT log_value FROM minido_log WHERE \
                log_value LIKE '23 " + addr_up   + ' __ __ __' + ' __' * int(idx_up)   + " ff %' OR \
                log_value LIKE '23 " + addr_down + ' __ __ __' + ' __' * int(idx_down) + " ff %' \
                ORDER BY log_time DESC LIMIT 1;")
        storesList[key][2] = 'down'
        for row in cur.fetchall():
            if str(row[0].split(' ')[5+int(idx_up)]).lower() == 'ff':
                storesList[key][2] = 'up'
        
        
def init_conditions():
    # Si maj conditions alors: init_conditions() + init_actions() + init_security_actions_conditions()
    global ConditionsList, ConditionsKeys
    cur = conn.cursor()
    ConditionsList = {}
    cur.execute("SELECT type, name, status, cond_order, cond_number FROM conditions ORDER BY cond_order ")
    for row in cur.fetchall():
        ConditionsList['C-'+str(row[4])] = [ str(row[0]), str(row[1]), int(row[2]), 'off', 0.0, int(row[3]), [], 0.0, ]
    cur.execute("SELECT condition_rowid, type, value1, value2 FROM condition_arguments ORDER BY type ")
    for row in cur.fetchall():
        ConditionsList['C-'+str(row[0])][6].append([ str(row[1]), str(row[2]), str(row[3]),])
    
    # Conditions  [ key ] = [ type,    name          , status, value, timestamp, order, [arguments: (type,value1,value2), ], previous_timestamp,]
    #ConditionsList['C-1'] = [ 'Tempo', 'BalisageHall', 1,      'off', 0.0,       10,    [['#1#timestamp-duration', '0.0', '40.0'], ['alarm', '5-7', ''], ], 0.0, ]
    #ConditionsList['C-2'] = [ 'And'  , 'Test and'    , 1,      'off', 0.0,       20,    [['key', 'I-17-6', ''], ['exo', '3f-5-on', ''], ], 0.0, ]
    #ConditionsList['C-3'] = [ 'And'  , 'BalisageHall', 1,      'off', 0.0,       30,    [['condition', 'C-1-on', ''], ['exo', '3f-0-on', ''], ], 0.0, ]
    #ConditionsList['C-4'] = [ 'And'  , 'Test after'  , 1,      'off', 0.0,       40,    [['alarm', '6-3', ''], ['after', 'A-6-3', '5.0'], ], 0.0, ]
    #ConditionsList['C-6'] = [ 'And'  , 'Sonnette2'   , 1,      'off', 0.0,       50,    [['condition', 'C-5-off', ''], ['key', 'I-16-11', ''], ], 0.0, ]
    #ConditionsList['C-5'] = [ 'Tempo', 'Sonnette1'   , 1,      'off', 0.0,       60,    [['#1#timestamp-duration', '0.0', '0.7'], ['key', 'C-6-on', ''], ], 0.0, ]
    #ConditionsList['C-7'] = [ 'Tempo', 'Sonnette3'   , 1,      'off', 0.0,       70,    [['#1#timestamp-duration', '0.0', '0.4'], ['key', 'C-6-on', ''], ], 0.0, ]
    
    ConditionsKeys = ConditionsList2sorted_keys()

    
def init_security_actions_conditions():
    # a utiliser, entre autre, apres maj init_actions().
    global ConditionsList, ConditionsKeys
    i=0
    # Securite concernant les volets
    for key in storesList:
        # print(str(storesList[key]))
        # Arret automatique apres x secondes de Up
        key2 = devicesList[storesList[key][0]][0]+'-'+devicesList[storesList[key][0]][1]
        ConditionsList['S-'+str(i)] = [ 'And'  , 'Store+'+key2    , 1,      'off', 0.0,     10000+i,    [['exo', key2+'-on', ''], ['after', 'O-'+key2, config['Store_Duration']], ], 0.0, ]
        action = ['',str(i),'ON',devicesList[storesList[key][0]][0],devicesList[storesList[key][0]][1],'0','OFF','0', ]
        add_Action_ActionsList('S-'+str(i)+'-on',action)
        
        # Arret automatique apres x secondes de Down
        i += 1
        key3 = devicesList[storesList[key][1]][0]+'-'+devicesList[storesList[key][1]][1]
        ConditionsList['S-'+str(i)] = [ 'And'  , 'Store+'+key3    , 1,      'off', 0.0,     10000+i,    [['exo', key3+'-on', ''], ['after', 'O-'+key3, config['Store_Duration']], ], 0.0, ]
        action = ['',str(i),'ON',devicesList[storesList[key][1]][0],devicesList[storesList[key][1]][1],'0','OFF','0', ]
        add_Action_ActionsList('S-'+str(i)+'-on',action)
        
        # Arret automatique si up et down en meme temps
        i += 1
        ConditionsList['S-'+str(i)] = [ 'And'  , 'Store+'+key2+'+'+key3, 1, 'off', 0.0,     10000+i,    [['exo', key2+'-on', ''], ['exo', key3+'-on', ''], ], 0.0, ]
        action = ['',str(i),'ON',devicesList[storesList[key][0]][0],devicesList[storesList[key][0]][1],'0','OFF','0', ]
        add_Action_ActionsList('S-'+str(i)+'-on',action)
        action = ['',str(i),'ON',devicesList[storesList[key][1]][0],devicesList[storesList[key][1]][1],'0','OFF','0', ]
        add_Action_ActionsList('S-'+str(i)+'-on',action)
        i += 1
        
        # Maj derniere action du Store
        action = [devicesList[storesList[key][0]][0],devicesList[storesList[key][0]][1],'ON',key,'up','0','MU_LAST_ACTION_STORE','0', ]
        add_Action_ActionsList('O-'+devicesList[storesList[key][0]][0]+'-'+devicesList[storesList[key][0]][1]+'-on',action)
        action = [devicesList[storesList[key][1]][0],devicesList[storesList[key][1]][1],'ON',key,'down','0','MU_LAST_ACTION_STORE','0', ]
        add_Action_ActionsList('O-'+devicesList[storesList[key][1]][0]+'-'+devicesList[storesList[key][1]][1]+'-on',action)
    
    # Securite ecran de projection
    for key in devicesList:
        if devicesList[key][2][0:6] == 'Screen':
            ConditionsList['S-'+str(i)] = [ 'And', devicesList[key][2], 1, 'off', 0.0, 10000+i, [['exo', key+'-on', ''], ['after', 'O-'+key, '0.5'], ], 0.0, ]
            action = ['',str(i),'ON',devicesList[key][0],devicesList[key][1],'0','OFF','0', ]
            add_Action_ActionsList('S-'+str(i)+'-on',action)
            i += 1
    i=0
    # (pas securite) duree minimale d'activation d'un capteur de mouvement
    for key in sensorsList:
        if sensorsList[key][2] == 'Mouvement':
            ConditionsList['C-A-'+key] = [ 'Tempo', sensorsList[key][3], 1, 'off', 0.0,   100000+i,    [['#1#timestamp-duration', '0.0', config['Sensor_Duration']], ['alarm', key, ''], ], 0.0, ]
            i += 1
            
    ConditionsKeys = ConditionsList2sorted_keys()
    
    
def init_lists():
    global statusList, newStatut, statusForceUpdate
    cur = conn.cursor()

    circularList_create('MonitoredEvents',32)
    
    init_config()
    init_buttons()
    init_sensors()
    init_devices()
    init_actions()
    init_conditions()
    init_security_actions_conditions()
    
    statusList_statusDB = {}
    statusList_timestamp = {}
    statusForceUpdate = {}
    
    cur.execute("SELECT module_bus, module_addr, last_status, last_update, last_src FROM minido_status WHERE module_bus = 'Default' ORDER BY module_addr ")
    for row in cur.fetchall():
        key = str(row[1])
        status_t = row[2].split(" ")
        statusList_statusDB[key] = [str(status_t[0]), str(status_t[1]), str(status_t[2]), str(status_t[3]), str(status_t[4]), str(status_t[5]), str(status_t[6]), str(status_t[7]), ]
        statusList_timestamp[key] = [int(time.time()), int(time.time()), int(time.time()), int(time.time()), int(time.time()), int(time.time()), int(time.time()), int(time.time())]
        statusForceUpdate[key] = 0
    statusList = [statusList_statusDB, statusList_timestamp,]
    
    newStatut = copy.deepcopy(statusList[0])
    
    
    
    
    
    

def Arg_evaluation(arg):
    if   arg[0] == 'alarm':
        if arg[1] in sensorsList and sensorsList[arg[1]][4]!=0:
            return 'on'
    elif arg[0] == 'key':
        if arg[1] in List_Key:
            return 'on'
    elif arg[0] == 'exo':
        (addr, idx, value) = arg[1].split('-')
        if addr != 'fe':
            if newStatut[addr][ int(idx) ] == command2value('',value):
                return 'on'
        else:
            if int(idx) in VirtualEXO:
                return VirtualEXO[int(idx)]
    elif arg[0] == 'condition':
        (type, idx, value) = arg[1].split('-')
        if type+'-'+idx in ConditionsList and ConditionsList[type+'-'+idx][3]==value:
            return 'on'
    elif arg[0] == 'after':
        (type, addr, idx) = arg[1].split('-')
        key = addr+'-'+idx
        if type == 'O' and addr in statusList[0] and statusList[0][addr][int(idx)]==newStatut[addr][int(idx)] and time.time()-statusList[1][addr][int(idx)] > float(arg[2]): # exo
            return 'on'
        if type == 'I' and key in buttonsList and time.time()-buttonsList[key][1] > float(arg[2]): # exi
            return 'on'
        if type == 'A' and key in sensorsList and time.time()-sensorsList[key][7] > float(arg[2]): # alarm
            return 'on'
    elif arg[0] == 'store':
        if arg[1] in storesList and storesList[arg[1]][2]==arg[2]:
            return 'on'
    return 'off'
    
def Conditions_evaluation():
    global ConditionsList, List_Key
    for key in ConditionsKeys:
      if key in ConditionsList:  # Ajoute suite a un plantage etrange
        if ConditionsList[key][2] > 0: # condition enabled
            newvalue = 'off'
            if ConditionsList[key][0] == 'Or':
                for arg in ConditionsList[key][6]:
                    if Arg_evaluation(arg)=='on':
                        newvalue = 'on'
                        break
            elif ConditionsList[key][0] == 'Tempo':
                for arg in ConditionsList[key][6]:
                    if Arg_evaluation(arg)=='on':
                        newvalue = 'on'
                        ConditionsList[key][6][0][1] = time.time() + float(ConditionsList[key][6][0][2])
                        break
                if newvalue == 'off' and float(ConditionsList[key][6][0][1]) >= time.time():
                    newvalue = 'on'
            elif ConditionsList[key][0] == 'And':
                newvalue = 'on'
                for arg in ConditionsList[key][6]:
                    if Arg_evaluation(arg)=='off':
                        newvalue = 'off'
                        break
            if ConditionsList[key][3] != newvalue:
                ConditionsList[key][3] = newvalue
                ConditionsList[key][7] = ConditionsList[key][4]
                ConditionsList[key][4] = time.time()
                List_Key.append(key+'-'+newvalue)
    return
    
def ConditionsList2sorted_keys():
    unsorted = []
    keys = []
    keys2 = []
    for key, condition in ConditionsList.iteritems():
        unsorted.append(condition[5])
        keys.append(key)
    sorted = copy.deepcopy(unsorted)
    sorted.sort()
    for order in sorted:
        keys2.append(keys[unsorted.index(order)])
    return keys2

def timestamp2duration(timestamp):
    if timestamp < 10: return 'unknow'
    duration = time.time() - timestamp
    if duration < 10: duration = round(duration,1)
    else:             duration = int(round(duration))
    (m,s) = divmod(duration, 60)
    (h,m) = divmod(m, 60)
    (d,h) = divmod(h, 24)
    result = str(s)+'s'
    if m>0.0: result = str(int(m))+'m ' + result
    if h>0.0: result = str(int(h))+'h ' + result
    if d>0.0: result = str(int(d))+'d ' + result
    return result

def print_message(level,source, message):
    
    if level<=DEBUG:
        if source!='':
            color='0'
            if   source == 'C. action': color='36'
            elif source == 'BusAlarme': color='35'
            elif source == 'KEYaction': color='34'
            elif source == 'BusMinido': color='33'
            elif source == 'WebdoSock': color='32'
            elif source == 'MU Debug' : color='31'
            elif source == 'MU Error' : color='31'
            msg_file   = str(datetime.datetime.now())[0:22] + " " + str(source).ljust(9) + " : " + message
            msg_screen = str(datetime.datetime.now())[11:22] + "\033["+color+'m ' + str(source).ljust(9) + "\033[0m : " + message
        else:
            msg_file   = message
            msg_screen = msg_file
        print(msg_screen)
        if config['LOG_MUWATCH'] == 'on':
            logfile_mu_watch.write(msg_file+'\n')
            logfile_mu_watch.flush()
    return ''

    
def to_hex(s):
    # Convert a character byte to hexadecimal number
    lst = []
    for ch in s:
        hv = hex(ord(ch)).replace('0x', '')
        lst.append(hv.zfill(2))
    return reduce(lambda x, y:x + y, lst)
    
    
def log2file(sercontent):
    
    if config['LOG_MINIDO']!='on': return
    
    global logfile
    for char in sercontent:
        char_hex = to_hex(char)
        if char_hex == '23':
            logfile.write('\n')
            logfile.write(str(datetime.datetime.now()) + ' : ')
        else:
            logfile.write(' ')
        logfile.write(char_hex)
        logfile.flush()

def log2file_c2000(sercontent):
    
    if config['LOG_C2000']!='on': return
    
    global logfile_c2000
    for char in sercontent:
        char_hex = to_hex(char)
        if char_hex == '23':
            logfile_c2000.write('\n')
            logfile_c2000.write(str(datetime.datetime.now()) + ' : ')
        else:
            logfile_c2000.write(' ')
        logfile_c2000.write(char_hex)
        logfile_c2000.flush()
        
def db_log(cmd_a):
    cur = conn.cursor()
    s_datas = " ".join(cmd_a)
    l_datas = (s_datas, )
    try:
        cur.execute("INSERT INTO minido_log (log_time,log_value) VALUES (datetime('now', 'localtime'), ? )", l_datas)
    except:
        print_message(0,'MU Error', 'An unexpected error occured while inserting into the DB : ' + str(cmd_a) + ' ' + str(l_datas) + " \n" + str(sys.exc_info()) )
    pass

def update_Lists(cmd_a):
    global newStatut, statusList, buttonsList, List_Key

    # last_src, new_status, module_addr,
    new_status = " ".join(cmd_a[5:13])
    l_datas = (cmd_a[2], new_status, cmd_a[1],)
    cur = conn.cursor()
    if cmd_a[4] == "01":
        # We have an EXO packet.
        if not cmd_a[1] in statusList[0]:
            print_message(0,'BusMinido', 'New device (addr: ' + str(cmd_a[1]) + '). Inserting it into the DB')
            cur.execute("INSERT INTO minido_status (module_bus, last_src, last_status, last_update, module_addr)\
                VALUES ('Default', ?, ?, datetime('now', 'localtime'), ? )", l_datas)
            init_lists()
        else:
            old_status_t = copy.deepcopy(newStatut[cmd_a[1]])
            new_status_t = new_status.split(" ")
            
            newStatut[cmd_a[1]]     = copy.deepcopy(new_status_t) # on maj puis on affichera les differences... et ajoutera la "key" a la liste.
            statusList[0][cmd_a[1]] = copy.deepcopy(new_status_t) # on maj statutList car vient du bus: changements deja effectifs
            
            # Update the DB
            cur.execute("UPDATE minido_status SET module_bus = 'Default', last_update = datetime('now', 'localtime'), \
                last_src = ?, last_status = ? WHERE module_addr = ?", l_datas)
            for i in range(0, 8):
                if new_status_t[i] != old_status_t[i]:
                    src_command = value2command(new_status_t[i])
                    key = cmd_a[1]+'-'+str(i)
                    if key in devicesList:
                        print_message(0,'BusMinido', devicesList[key][3] + " : " + devicesList[key][4] + " " + devicesList[key][2] + " ("+timestamp2duration(statusList[1][cmd_a[1]][i])+") : " + src_command)
                    else:
                        print_message(0,'BusMinido', 'The Output on EXO ' + cmd_a[1] + " / Index " + str(i) + " changed from " + old_status_t[i] + " to " + new_status_t[i])
                    statusList[1][cmd_a[1]][i] = int(time.time())
                    key = 'O-' + cmd_a[1] + '-' + str(i) + '-' + src_command
                    List_Key.append(key)
        
    elif cmd_a[4] == '31' and cmd_a[3] == '04':
        # We have an EXI packet
        key = cmd_a[2] + '-' + cmd_a[6]
        if key in buttonsList and buttonsList[key] != '' and buttonsList[key][0] != 'None':
            print_message(0,'BusMinido', 'Button ' + buttonsList[key][0] + ' has been pressed')
            buttonsList[key][1] = time.time()
        else:
            print_message(0,'BusMinido', 'Button (EXI: ' + cmd_a[2] + ' - number: ' + cmd_a[6] + ' - Mode:' + cmd_a[5] + ') has been pressed')
        key = key_lstrip_zero('I',cmd_a[2],cmd_a[6]) # .lstrip('0') afin que, par ex, 06 -> 6
        List_Key.append(key)
        
        # Populate the DB.
        cur.execute("UPDATE minido_buttons SET last_event = datetime('now') \
            WHERE exi_addr = ? AND button_type = ? AND button_addr = ?", ( cmd_a[2], cmd_a[5], cmd_a[6] ))
        if cur.rowcount < 1:
            cur.execute("INSERT INTO minido_buttons (exi_addr, button_type, button_addr, last_event)\
                VALUES ( ?, ?, ?, datetime('now', 'localtime') )", ( cmd_a[2], cmd_a[5], cmd_a[6] ))
    
    elif cmd_a[4] == '31' and cmd_a[3] == '0d' and cmd_a[15] == '03':
        # EXI long
        key = str(cmd_a[2]+"-"+str(cmd_a[6]))
        if key in buttonsList and str(buttonsList[key]) != '' and buttonsList[key][0] != 'None' and time.time()-buttonsList[key][1]>float(config['BUTTON_LONG_PRESS']):
            print_message(0,'BusMinido', 'Button ' + buttonsList[key][0] + ' has been released - long press')
            key = key_lstrip_zero('L',cmd_a[2],cmd_a[6]) # .lstrip('0') afin que, par ex, 06 -> 6
            List_Key.append(key)
    return


def minido_get_hex_checksum(exo_byte_a):
    # exo_byte_a = exopkt.split(" ", 20)
    chksum = 0
    for exo_byte in exo_byte_a:
        chksum = chksum ^ int( exo_byte, 16 )
    hexchksum = hex(chksum).replace('0x','')
    return hexchksum.zfill(2)


def invalid_packet(cmd_a):
    if len(cmd_a) < 7:
        # The packet is too short. This is common when checking before the end of transmission.
        return 1
    if cmd_a[0] != '23':
        print_message(3,'MU Debug', 'Packet does not start with 0x23')
        return 1
    if cmd_a[len(cmd_a)-1] != minido_get_hex_checksum(cmd_a[4:len(cmd_a)-1]):
        print_message(3,'MU Debug', 'Checksum Error : ' + cmd_a[len(cmd_a)-1] + ' != ' + minido_get_hex_checksum(cmd_a[4:len(cmd_a)-1]))
        return 1
    if cmd_a[4] == '01' or cmd_a[4] == '31':
        return 0
    print_message(0,'MU Debug', 'Unexpected invalid packet. This should never happen.')
    return 1

    
def command2value(oldvalue,command):
    cmd =  str(command).lower()
    if   cmd == "on":                          return 'ff'
    elif cmd == "off":                         return '00'
    elif oldvalue == "00" and cmd == "toggle": return 'ff'
    elif oldvalue != "00" and cmd == "toggle": return '00'
    elif re.match("[0-9,a-f]{2}$", cmd):       return cmd

        
def value2command(value):
    cmd = str(value).lower()
    if cmd == '00' or cmd == '0': return 'off'
    if cmd == 'ff':               return 'on'
    return cmd

    
def ChangeStatusEXO(addr,idx,cmd,source):
    global newStatut, VirtualEXO, List_Key
    cmd2 = value2command(cmd)
    if   addr=='ff':
        process_special_action(idx,cmd)
    elif addr=='fe':
        oldvalue='00'
        if int(idx) in VirtualEXO: oldvalue=VirtualEXO[int(idx)]
        cmd3 = command2value(oldvalue,cmd2)
        cmd4 = value2command(cmd3)
        key = 'V-fe-' + str(idx) + '-' + cmd4
        VirtualEXO[int(idx)] = cmd4
        if cmd2 == 'toggle': cmd4 += ' (toggle)'
        print_message(0, source, 'Virtual EXO ' + str(idx) + " changed to " + cmd4)
        List_Key.append(key)
    elif addr in statusList[0]:
        newStatut[addr][ int(idx) ] = command2value(newStatut[addr][ int(idx) ],cmd2)
        key = addr + '-' + str(idx)
        if key in devicesList:
            dev = devicesList[key]
            if cmd2 == 'toggle': cmd3 = value2command(newStatut[addr][int(idx)])+' (toggle)'
            else:                cmd3 = cmd2
            print_message(0, source, dev[2] + " - "  + dev[3] + " - " + dev[4] + " ("+timestamp2duration(statusList[1][addr][int(idx)])+") : " + cmd3 )
        else:
            print_message(0, source, 'The Output on EXO ' + addr + " / Index " + str(idx) + " changed to " + newStatut[addr][int(idx)])
        List_Key.append('O-'+key+'-'+value2command(newStatut[addr][int(idx)])) # pas cmd2 car prob avec toggle
    
    return
    
    
def store(source,addr,idx):
    key = addr+'-'+idx
    if key in devicesList and devicesList[key][3]+"#"+devicesList[key][4] in storesList:
        key2 = devicesList[key][3]+"#"+devicesList[key][4]
        StoreUp   = devicesList[storesList[key2][0]]
        StoreDown = devicesList[storesList[key2][1]]
        if newStatut[StoreUp[0]][ int(StoreUp[1]) ] != '00' or newStatut[StoreDown[0]][ int(StoreDown[1]) ] != '00':
            if newStatut[StoreUp[0]][ int(StoreUp[1]) ]     != '00': ChangeStatusEXO(StoreUp[0],StoreUp[1],'off',source)
            if newStatut[StoreDown[0]][ int(StoreDown[1]) ] != '00': ChangeStatusEXO(StoreDown[0],StoreDown[1],'off',source)
        else:
            if storesList[key2][2] == 'up': ChangeStatusEXO(StoreDown[0],StoreDown[1],'on',source)
            else:                           ChangeStatusEXO(StoreUp[0],StoreUp[1],'on',source)


def key2source(key):
    if key[0]=='A':  return 'A. action'
    if key[0]=='I':  return 'EXIaction'
    if key[0]=='O':  return 'EXOaction'
    if key[0]=='V':  return 'EXOvirtua'
    if key[0]=='L':  return 'EXIaction'
    if key[0]=='C':  return 'C. action'
    return 'KEYaction'
    

def process_actions_trigger():
    global ActionsList, devicesList, futur_actions_List, storesList
    for key in List_Key:
      
      if key[0:4] == 'C-A-':
          (c, a, byte, bit, cmd) = key.split('-')
          print_message(0, 'BusAlarme', ConditionsList['C-A-'+byte+'-'+bit][1] + ' ('+timestamp2duration(ConditionsList['C-A-'+byte+'-'+bit][7])+') : '+cmd)
      
      if key in ActionsList:
        cur = conn.cursor()
        source = key2source(key)
        for action in ActionsList[key]:
            addr     = action[3]
            idx      = action[4]
            delay    = action[5]
            cmd      = action[6]
            duration = action[7]
            if len(cmd) >= 8: # la longueur de la commande est grande: il s'agit d'autre chose qu'un on/off/toggle
                arg = shlex.split(str(cmd))
                if   arg[0] == 'MU_STORE':
                    store(source,addr,idx)
                elif arg[0] == 'KEY_MONITORED':
                    circularList_add('MonitoredEvents',(key,time.time()))
                elif arg[0] == 'MU_LAST_ACTION_STORE':
                    storesList[addr][2] = idx # addr contient la key et idx 'up' ou 'down'
                else:
                    try:
                        print_message(0, source, 'Script exec: ' + cmd.lower())
                        subprocess.Popen(arg)
                    except:
                        print_message(0, 'MU Error', 'Error while running script exec: ' + cmd.lower() + " \n" + str(sys.exc_info()))
            else: # il s'agit d'un on/off/toggle
                if addr+"-"+idx in devicesList: dev = devicesList[ addr+"-"+idx]
                else:                           dev = [ '', '', 'Virtual EXO', addr, idx, ]
                
                if int(delay)>0: # il y a un delai, on passe par le scheduler
                    print_message(0, source, '(in ' + delay + ' seconds) ' + dev[2] + " - "  + dev[3] + " - " + dev[4] + " (" + addr + "," + idx + ") : " + cmd.lower())
                    cur.execute("INSERT INTO exo_scheduler (exo_addr, idx, s_time, command) \
                        VALUES ( ?, ?, datetime('now', ?, 'localtime'), ?)", (addr, idx, delay + " seconds", cmd ))
                elif int(delay)<0: # il y a un delai, exprime en cycle, on passe par la liste futur_actions_List
                    print_message(0, source, '(in ' + str(-int(delay)) + ' cycles) ' + dev[2] + " - "  + dev[3] + " - " + dev[4] + " (" + addr + "," + idx + ") : " + cmd.lower())
                    futur_actions_List.append( [-int(delay), source, addr, idx, cmd, ] )
                else:
                    ChangeStatusEXO(addr,idx,cmd,source)
                    
                if int(duration) > 0:
                    command = cmd
                    if cmd == 'ON':    command = 'OFF'
                    elif cmd == 'OFF': command = 'ON'
                    print_message(0, source, '(in ' + str(int(delay) + int(duration)) + " seconds) " + dev[2] + " - "  + dev[3] + " - " + dev[4] + " (" + addr + "," + idx + ") : " + cmd.lower() )
                    cur.execute("INSERT INTO exo_scheduler (exo_addr, idx, s_time, command) \
                        VALUES ( ?, ?, datetime('now', ?, 'localtime'), ?)", (addr, idx, str(int(delay) + int(duration)) + " seconds", command ))


def process_schedules():
    cur = conn.cursor()
    cur.execute("SELECT exo_addr, idx, command, source, rowid FROM exo_scheduler \
        WHERE processed IS NULL AND s_time <= datetime('now', 'localtime');" )
    for row in cur.fetchall():
        exo_addr = str(row[0]).lower()
        idx = row[1]
        command = str(row[2]).strip()
        source = str(row[3]).strip()
        rowid = row[4]
        if source=='' or source=='None':
            source = 'Scheduler'
        ChangeStatusEXO(exo_addr,idx,command,source)
        # Update the DB
        cur.execute("UPDATE exo_scheduler SET processed = datetime('now', 'localtime') WHERE rowid = ?", (rowid, ))
        
        
def process_special_action(idx,cmd):
    global DEBUG, Active, statusForceUpdate
    
    command = cmd.upper()
    arg = command.split(" ")
    if idx==0:
        if   command == 'EXIT':
            cur = conn.cursor()
            cur.execute("UPDATE exo_scheduler SET processed = datetime('now', 'localtime') WHERE command = 'EXIT'")
            conn.commit()
            print_message(0, 'MU Info', 'Exit!')
            Active = False
            #sys.exit()
        elif command == 'RELOAD':
            print_message(0, 'MU Info', 'DB reloaded (init_lists())')
            init_lists()
        elif command == 'RELOAD_CONFIG':
            print_message(0, 'MU Info', 'Config reloaded (init_config())')
            init_config()
        elif command == 'RELOAD_BUTTONS':
            print_message(0, 'MU Info', 'Buttons list reloaded (init_buttons())')
            init_buttons()
        elif command == 'RELOAD_SENSORS':
            print_message(0, 'MU Info', 'Sensors list reloaded (init_sensors())')
            init_sensors()
        elif command == 'RELOAD_ACTIONS':
            print_message(0, 'MU Info', 'Actions list reloaded (init_actions())')
            init_actions()
            init_security_actions_conditions()
        elif len(arg) >= 1:
            if str(arg[0]) == 'DEBUG':
                if len(arg) > 1:
                    DEBUG = int(str(arg[1]))
                    print_message(0, 'MU Debug', 'Mode debug: level ' + str(arg[1]))
                else:
                    print_message(0, 'MU Debug', 'Info: debug level ' + str(DEBUG))
            elif str(arg[0]) == 'CONDITIONS':
                if len(arg) > 2:
                    for key in ConditionsKeys:
                        if ConditionsList[key][1].upper()==str(arg[1]):
                            if int(str(arg[2])) == 0:
                                ConditionsList[key][2] = 0
                            else:
                                ConditionsList[key][2] = 1
                            print_message(0, 'MUConfig', "Condition: " + key + " : " + str(ConditionsList[key]) )
                else:
                    for key in ConditionsKeys:
                        print_message(0, '', "Condition: " + key + " : " + str(ConditionsList[key]) )
            elif str(arg[0]) == 'RADIATOR':
                for radiator in radiatorsList:
                    print_message(0, '', "Radiator: " + str(radiator) )
            elif str(arg[0]) == 'ALARM':
                for sensor in sensorsList:
                    print_message(0, '', "Sensor: " + str(sensorsList[sensor]) )
            elif str(arg[0]) == 'ACTIONS':
                for key, action in ActionsList.iteritems():
                    print_message(0, '', "Action: " + key + " -> " + str(action) )
            elif str(arg[0]) == 'UPTIME':
                print_message(0, '', "Uptime: " + timestamp2duration(UPTIME))
            elif str(arg[0]) == 'STATUS':
                    print_message(0, '', "Statuts: " + str(statusList) + ' \n\nVirtual Exo: ' + str(VirtualEXO) )
            elif str(arg[0]) == 'CONFIG':
                for key, value in config.iteritems():
                    print_message(0, '', "config: " + key + " -> " + str(value) )
            elif str(arg[0]) == 'STORES':
                for key, store in storesList.iteritems():
                    print_message(0, '', "Store: " + key + " -> " + str(store) )
            elif str(arg[0]) == 'FORCE_UPDATE':
                exo_addr = arg[1].lower()
                if exo_addr in statusForceUpdate:
                    statusForceUpdate[exo_addr] = 1
                    print_message(0, 'Scheduler', 'Force update: resend status for exo ' + exo_addr)
    elif idx==1:
        try:
            arg2 = shlex.split(str(cmd))
            print_message(0, 'Scheduler', 'Script exec: ' + str(cmd))
            subprocess.Popen(arg2)
        except:
            print_message(0, 'MU Error', 'Error while running script exec: ' + str(cmd) + " \n" + str(sys.exc_info()))


def retrieve_data_from_bus_minido():
    global rdbuffer_minido
    data = ""
    if( config['CONNTYPE_MINIDO'] == "Socket"):
        try:
            data = PySocket.recv(2048)
        except( socket.timeout ):
            pass
        else:
            rdbuffer_minido.extend(data)
            log2file(data)
    else:
        n = 0
        try:
            n = ser.inWaiting()
            if n > 0:
                sercontent = ser.read(n)
        except:
            print_message(0, 'Error', 'Serial - function retrieve_data_from_bus_minido' + " \n" + str(sys.exc_info()))
        else:
            if n > 0:
                rdbuffer_minido.extend(sercontent)
                log2file(sercontent)
    return
    
def retrieve_command_from_buffer_minido():
    global rdbuffer_minido
    lenrdb = len(rdbuffer_minido)
    if lenrdb > 0:
        if rdbuffer_minido[0] != "#": #  code ascii de '#' = 23
            print_message(1, 'MU Debug', 'rdbuffer_minido does not start with 0x23, I must delete the first character.')
            del rdbuffer_minido[0:1]
    if lenrdb > 7:
        # Stripe the begining of the list up to 0x23.
        try:
            startidx = rdbuffer_minido.index('23'.decode("hex"))
        except:
            # We are not interested in data not starting by 0x23.
            print_message(3, 'MU Debug', 'Error : The buffer contains more than 7 characters, but none is 0x23. Dropping everything.')
            db_log(rdbuffer_minido)
            rdbuffer_minido = list()
            return ''

        if  startidx > 0:
            print_message(1, 'MU Debug ', 'The buffer was starting by ' + str(hex(ord(rdbuffer_minido[0]))) + ' instead of expected 0x23. Stripping ' + str(startidx) + ' character(s)')
            db_log(map(to_hex, rdbuffer_minido[0:startidx]))
            del rdbuffer_minido[0:startidx]
            if len(rdbuffer_minido) < 10:
                return ''

        dtlength = int(ord(rdbuffer_minido[3]))
        if len(rdbuffer_minido) >= dtlength + 4:
            cmd = rdbuffer_minido[0:(dtlength + 4)]
            cmd_a = map(to_hex, cmd)
            db_log(cmd_a)
            del rdbuffer_minido[0:(dtlength + 4)]
            if  invalid_packet(cmd_a):
                print_message(0, 'MU Debug', 'Invalid command : ' + str(cmd_a))
            else:
                return cmd_a
    return ''

def update_DB():
    global newStatut, statusList, statusForceUpdate
    cur = conn.cursor()
    for addr in statusList[0]:
        update = 0
        for i in range(0, 8):
            if newStatut[addr][i]!=statusList[0][addr][i]:
                update = 1
                statusList[1][addr][i] = int(time.time())
        if update==1 or statusForceUpdate[addr]==1:
            # The 22 is the 0x22 address of the sender EXI. 0x22 is not used in my EXIs.
            exopkt = '23 ' + addr + ' 22 0a 01 ' + ' '.join(newStatut[addr]) + ' ' + minido_get_hex_checksum(['01'] + newStatut[addr])
            print_message(1, 'MUscript ', 'Sending packet  ' + exopkt)
            db_log((exopkt, ))
            exopkt = exopkt.replace(' ','')
            # Send packet to bus
            if( config['CONNTYPE_MINIDO'] == 'Socket'):
                try:
                    PySocket.send(exopkt.decode("hex"))
                except:
                    print_message(0, 'Error', 'PySocket.send failed!' + " \n" + str(sys.exc_info()))
                    # FIXME: recreate a new connection to the socket
                    sys.exit(1)
            else:
                try:
                    ser.write(exopkt.decode("hex"))
                except:
                    print_message(0, 'Error', 'ser.write failed!' + " \n" + str(sys.exc_info()))

            # Update the DB
            cur.execute("UPDATE minido_status SET last_status = ?, last_update = datetime('now', 'localtime'), last_src = 0 WHERE module_addr = ?", (" ".join(newStatut[addr]), addr,))
            statusList[0][addr] = copy.deepcopy(newStatut[addr])
            statusForceUpdate[addr] = 0
    return
    
def retrieve_data_from_bus_c2000():
    global rdbuffer_c2000
    if   (config['CONNTYPE_C2000'] == 'Socket'):
        try:
            data = Socket_C2000.recv(2048)
        except( socket.timeout ):
            pass
        else:
            rdbuffer_c2000.extend(data)
            log2file_c2000(data)
    elif (config['CONNTYPE_C2000'] == 'Serial'):
        n = 0
        try:
            n = ser2.inWaiting()
            if n > 0:
                sercontent = ser2.read(n)
        except:
            print_message(0, 'Error', 'Serial - function retrieve_data_from_bus_c2000' + " \n" + str(sys.exc_info()))
        else:
            if n > 0:
                rdbuffer_c2000.extend(sercontent)
                log2file_c2000(sercontent)
    return

def retrieve_command_from_buffer_c2000():
    global rdbuffer_c2000
    lenrdb = len(rdbuffer_c2000)
    if lenrdb > 5:
        if rdbuffer_c2000[0] != "#": #  code ascii de '#' = 23
            try:
                startidx = rdbuffer_c2000.index('#')
            except:
                print_message(0, 'BusAlarme', 'rdbuffer_c2000 does not contain 0x23, stripping ' + str(lenrdb) + ' characters')
                rdbuffer_c2000 = list()
                return ''
            if  startidx > 0:
                print_message(0, 'BusAlarme', 'rdbuffer_c2000 not starting with 0x23, stripping ' + str(startidx) + ' character(s)')
                del rdbuffer_c2000[0:startidx]
                if len(rdbuffer_c2000) < 6:
                    return ''

        # Try to extract a command, and remove it from rdbuffer_c2000 if complete.
        dtlength = int(ord(rdbuffer_c2000[3]))
        if len(rdbuffer_c2000) >= dtlength + 4:
            #cmd = [ord(i) for i in rdbuffer_c2000[0:dtlength + 4]]
            cmd = [ord(i) for i in rdbuffer_c2000[0:dtlength + 4]]
            del rdbuffer_c2000[0:(dtlength + 4)]
            return cmd

    return ''

    
def process_command_from_buffer_c2000(cmd):
    global cmd_old, sensorsList, last_time_cmd_c2000, Message_c2000, List_Key
    if len(cmd)<10: return # optimisation
    if len(cmd)==20 and cmd[1]==2 and cmd[2]==1 and cmd[3]==16:
        if cmd_old!=cmd: # and len(cmd_old)>0
            for i in range(4, 19):
                valeur = cmd[i] #int( "0x"+cmd[i], 16)
                valeur_old = cmd_old[i]
                for j in range(7,-1,-1):
                    poids = 2**j
                    if valeur<poids:
                        etat = "on"
                        etat2 = 1
                    else:
                        etat = "off"
                        etat2 = 0
                        valeur -= poids
                    if valeur_old<poids:
                        etat_old = "on"
                    else:
                        etat_old = "off"
                        valeur_old -= poids
                    if etat!=etat_old:
                        key = str(i)+"-"+str(j)
                        if key in sensorsList:
                            sensorsList[key][4] = etat2
                            if sensorsList[key][2] != 'Mouvement':
                                print_message(0, 'BusAlarme', sensorsList[key][3] + " (" + timestamp2duration(sensorsList[key][7]) + ") : " + etat)
                            sensorsList[key][7] = time.time()
                        else:
                            print_message(0, 'BusAlarme', key + " : " + etat)
                        
                        key = 'A-' + str(i) + '-' + str(j) + '-' + etat
                        List_Key.append(key)
            cmd_old = copy.deepcopy(cmd)
            
        last_time_cmd_c2000 = time.time()
    elif len(cmd)==23 and cmd[1]==13 and cmd[5]==0 and cmd[6:17]!=Message_c2000[0][0][0:11] :
        Message_c2000[0][0] = cmd[6:22]
        Message_c2000[0][1] = ""
        for i in cmd[6:22]:
            Message_c2000[0][1] += chr(i)
        print_message(0, 'BusAlarme', "New message, line 1: " + Message_c2000[0][1])
    elif len(cmd)==23 and cmd[1]==13 and cmd[5]==64 and cmd[6:22]!=Message_c2000[1][0] :
        Message_c2000[1][0] = cmd[6:22]
        Message_c2000[1][1] = ""
        for i in cmd[6:22]:
            Message_c2000[1][1] += chr(i)
        print_message(0, 'BusAlarme', "New message, line 2: " + Message_c2000[1][1])
    elif len(cmd)==11 and cmd[1]==13 and cmd[4:10]!=Message_c2000[2][0][0:6] :
        for i in range(4, 10):
            if cmd[i]!=Message_c2000[2][0][i-4]:
                key = "100-"+str(i-4)
                etat = "off"
                if int(cmd[i])>0: etat = "on"
                if key in sensorsList:
                    sensorsList[key][4] = int(cmd[i])
                    print_message(0, 'BusAlarme', sensorsList[key][3] + " (" + timestamp2duration(sensorsList[key][7]) + ") : " + etat)
                    sensorsList[key][7] = time.time()
                else:
                    print_message(0, 'BusAlarme', key + " : " + etat)
        Message_c2000[2][0] = cmd[4:10]
        #print_message(0, 'BusAlarme', "Informations: " + str(Message_c2000[2][0]))
    return ''
    

def heating_control():

    if config['Heating_Management']!='on': return ''

    global statusList, newStatut, radiatorsList
    
    #Start circulator?
    if newStatut[config['Heating_Circulateur_Addr']][int(config['Heating_Circulateur_Idx'])]=='00' or newStatut[config['Heating_Thermostat_Addr']][int(config['Heating_Thermostat_Idx'])]=='00':
        for key in radiatorsList:
            if statusList[0][radiatorsList[key][0]][int(radiatorsList[key][1])] == 'ff':
                newStatut[config['Heating_Thermostat_Addr']][int(config['Heating_Thermostat_Idx'])]   = 'ff'
                if (int(time.time())-statusList[1][radiatorsList[key][0]][int(radiatorsList[key][1])] > int(config['Heating_Delay'])):
                    newStatut[config['Heating_Circulateur_Addr']][int(config['Heating_Circulateur_Idx'])] = 'ff'
                    print_message(0, 'Heating  ', 'Circulator on!')
                    break
    
    #Stop circulator?
    for key in radiatorsList:
      if newStatut[radiatorsList[key][0]][int(radiatorsList[key][1])] == 'ff':
          break
    else:
        newStatut[config['Heating_Circulateur_Addr']][int(config['Heating_Circulateur_Idx'])] = '00'
        newStatut[config['Heating_Thermostat_Addr']][int(config['Heating_Thermostat_Idx'])]   = '00'
        if statusList[0][config['Heating_Circulateur_Addr']][int(config['Heating_Circulateur_Idx'])]!='00':
            print_message(0, 'Heating  ', 'All radiators are now off, circulator off!')
    return

def c2000_control():
    global last_time_cmd_c2000
    if (config['CONNTYPE_C2000'] == 'Serial' or config['CONNTYPE_C2000'] == 'Socket'):
        if time.time()-last_time_cmd_c2000>5:
            print_message(0, 'BusAlarme', 'Error, connection lost!')
            if (config['CONNTYPE_C2000'] == 'Serial'):
                ser2.close()
                subprocess.call(["/opt/minido-unleashed/mu_remserial.sh", "restart_c2000"])
                time.sleep(1)
                print_message(0, 'MUscript', 'remserial C2000 restarted')
                ser2.open()
                last_time_cmd_c2000 = time.time()+600
    return ''

def db_version():
    cur = conn.cursor()
    cur.execute("SELECT count(*) as nb FROM sqlite_master WHERE type = 'table' AND name='mu_config' ")
    row = cur.fetchall()
    if int(row[0][0]) > 0:
        cur.execute("SELECT value FROM mu_config WHERE name = 'Version_DB' LIMIT 1 ")
        row = cur.fetchall()
        if row <> []:
            return row[0][0]
        else:
            print ("Error: Database version unknow ('Version_DB' not available in mu_config) " )
            #sys.exit()
    return 1

def process_futur_actions_List():
    global futur_actions_List #, newStatut, devicesList
    for action in futur_actions_List:
        if action[0]<=0:
            ChangeStatusEXO(action[2],action[3],action[4],'(Delayed)') # affiche '(Delayed)' au lieu de source (action[1])
            futur_actions_List.remove(action)
        else:
            action[0] -= 1

            
def listen_socket_MU():
    global sensorsList, Active, ConditionsList, ConditionsKeys, statusForceUpdate
    try:
        conn_MU, addr_MU = Socket_MU.accept()
    except:
        return ''
        
    #print_message(0, 'MUsocket', 'Connected by' + str(addr_MU))
    try:
        cur = conn.cursor()
        while 1:
            data = conn_MU.recv(1024)
            if not data: break
            arg = data.split(' ')
            if   arg[0]=='EXIT':
                conn_MU.send('OK')
                print_message(0, 'MU Info', 'Exit!')
                conn.commit()
                Active = False
                #sys.exit()
            elif arg[0]=='ALARM':
                if config['CONNTYPE_C2000']=='None':
                    conn_MU.send('^')
                else:
                    message = ''
                    for sensor in sensorsList:
                         message += str(sensorsList[sensor][0]) + ',' + str(sensorsList[sensor][1]) + ',' + str(sensorsList[sensor][4]) + ',' + str(sensorsList[sensor][7]) + '^'
                    conn_MU.send(message.rstrip('^'))
            elif arg[0]=='UPTIME':
                conn_MU.send(str(time.time()-UPTIME))
            elif arg[0]=='ACTION' and len(arg)>4:
                exo_addr = arg[1].lower()
                idx = int(arg[2])
                command = arg[3]
                source = arg[4]
                if source=='' or source=='None':
                    source = 'MUsocket'
                ChangeStatusEXO(exo_addr,idx,command,source)
                conn_MU.send('OK')
            elif arg[0]=='STATUS':
                message = ''
                for key, value in statusList[0].iteritems():
                    message += key + "!" 
                    for value2 in value:
                        message += str(value2) + "!"
                message.rstrip('!')
                message += '^'
                for key, value in statusList[1].iteritems():
                    message += key + '!' 
                    for value2 in value:
                        message += str(value2) + '!'
                message.rstrip('!')
                conn_MU.send(message)
            elif arg[0]=='STATUS_VEXO':
                message = ''
                for key, value in VirtualEXO.iteritems():
                    message += str(key) + ',' + str(value) + '!'
                message.strip('!')
                if message == '': message = 'OK'
                conn_MU.send(message)
            elif arg[0]=='RELOAD':
                print_message(0, 'MU Info', 'DB reloaded (init_lists())')
                init_lists()
                conn_MU.send('OK')
            elif arg[0]=='RELOAD_CONFIG':
                print_message(0, 'MU Info', 'Config reloaded (init_config())')
                init_config()
                conn_MU.send('OK')
            elif arg[0]=='RELOAD_BUTTONS':
                print_message(0, 'MU Info', 'Buttons list reloaded (init_buttons())')
                init_buttons()
                conn_MU.send('OK')
            elif arg[0]=='RELOAD_SENSORS':
                print_message(0, 'MU Info', 'Sensors list reloaded (init_sensors())')
                init_sensors()
                conn_MU.send('OK')
            elif arg[0]=='RELOAD_SENSOR_DURATION':
                for key in ConditionsList:
                    if key[0:4] == 'C-A-':
                        ConditionsList[key][6][0][2] = config['Sensor_Duration']
                conn_MU.send('OK')
            elif arg[0]=='RELOAD_ACTIONS':
                print_message(0, 'MU Info', 'Actions list reloaded (init_actions())')
                init_actions()
                init_security_actions_conditions()
                conn_MU.send('OK')
            elif arg[0]=='RELOAD_CONDITIONS':
                print_message(0, 'MU Info', 'Conditions list reloaded (init_conditions())')
                init_conditions()
                init_actions()
                init_security_actions_conditions()
                conn_MU.send('OK')
            elif arg[0]=='RELOAD_CONDITIONS_ORDER':
                print_message(0, 'MU Info', 'Order, name and status conditions list reloaded')
                cur.execute("SELECT name, status, cond_order, cond_number FROM conditions ORDER BY cond_order ")
                for row in cur.fetchall():
                    ConditionsList['C-'+str(row[3])][1] = str(row[0])
                    ConditionsList['C-'+str(row[3])][2] = int(row[1])
                    ConditionsList['C-'+str(row[3])][5] = int(row[2])
                ConditionsKeys = ConditionsList2sorted_keys()
                conn_MU.send('OK')
            elif arg[0]=='RELOAD_CONDITIONS_ARG':
                print_message(0, 'MU Info', 'Conditions arguments reloaded')
                for key in ConditionsList:
                    if key[0:2] == 'C-' and key[0:4] != 'C-A-':
                        ConditionsList[key][6] = []
                cur.execute("SELECT condition_rowid, type, value1, value2 FROM condition_arguments ORDER BY type ")
                for row in cur.fetchall():
                    ConditionsList['C-'+str(row[0])][6].append([ str(row[1]), str(row[2]), str(row[3]),])
                conn_MU.send('OK')
            elif arg[0]=='STORES':
                conn_MU.send(str(storesList))
            elif arg[0]=='MONITORED_EVENT_LIST':
                message = str(circularList_view('MonitoredEvents'))
                conn_MU.send(message)
            elif arg[0]=='MONITORED_EVENT_CLEAR':
                message = str(circularList_view2('MonitoredEvents'))
                conn_MU.send(message)
            elif arg[0]=='MONITORED_EVENT_ALL':
                message = str(circularList_view_all('MonitoredEvents'))
                conn_MU.send(message)
            elif arg[0]=='FORCE_UPDATE':
                exo_addr = arg[1].lower()
                if exo_addr in statusForceUpdate:
                    statusForceUpdate[exo_addr] = 1
                    print_message(0, 'MUsocket', 'Force update: resend status for exo ' + exo_addr)
                conn_MU.send('OK')
            else:
                conn_MU.send('OK')
    except:
        print_message(0, 'MUsocket', 'Error!!!'  + " \n" + str(sys.exc_info()))
        pass
    try:
        conn_MU.close()
    except:
        print_message(0, 'MUsocket', 'Error closing socket!!!' + " \n" + str(sys.exc_info()))
        pass
    #print_message(0, 'MUsocket', 'Deconnected')


def process_rfid():
    global rdbuffer_rfid, List_Key, config
    
    if config['RFID'] != 'on': return
    
    try:
        n = ser_rfid.inWaiting()
        if n > 0:
            rdbuffer_rfid += str(ser_rfid.read(n))
    except:
            print_message(0, 'Error', 'Serial - function process_rfid' + " \n" + str(sys.exc_info()))
            pass
    else:
        if n > 0 and len(rdbuffer_rfid) >= 14:
            p = rdbuffer_rfid.rfind(chr(13)+chr(10)+chr(3))
            if p>10:
                tag = rdbuffer_rfid[p-10:p]
                cur = conn.cursor()
                if tag in config:
                    print_message(0, 'MU rfid', 'Tag detected: '+config[tag].split(';')[0]+' ('+tag+')')
                    datas = ( config[tag].split(';')[0]+';'+str(int(time.time())), tag, ) # maj dans la db du timestamp du tag
                    cur.execute("UPDATE mu_config SET value = ? WHERE name = ?", datas)
                else:
                    print_message(0, 'MU rfid', 'New tag detected: '+tag)
                    datas = (tag, 'Unknow;'+str(int(time.time())),) # insertion dans la db du nouveau tag
                    cur.execute("INSERT INTO mu_config (name, value) VALUES (?, ?)", datas)
                    config[tag] = 'Unknow;'+str(int(time.time()))
                List_Key.append('R-'+tag)
                rdbuffer_rfid = rdbuffer_rfid[p+2:]



# ############################## #
# Main program                   #
# ############################## #

# Connect to SQLite DB
try:
    conn = sqlite3.connect(sqlitedb)
except:
    print_message(0, '', "I am unable to connect to the SQLITE database ( " + sqlitedb + ") , exiting.")
    sys.exit()

# Version DB
version = db_version()
print ( "Database version: " + str(version) )
if float(version) < float(MIN_DB_VERSION):
    print ("Mu Error: Database version incompatible.")

# Initialisation de variables

Active = True

VirtualEXO = {}
circularLists = {}

rdbuffer_minido = list()
rdbuffer_c2000  = list()
rdbuffer_rfid   = ""

futur_actions_List = list() # structure: [ cycle(int), source(str), addr(str), idx(str), cmd(str), ]

Message_c2000 = [ [ [], "" ],   # 1er ligne : liste d'entiers + texte
                  [ [], "" ],   # 2e ligne  : idem
                  [ [ 0, 0, 0, 0, 0, 0, ], "" ] ]  # informations

cmd_old = [255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, ]
    
# Initialisation des donnees de la DB
init_lists()

# Initialisation timestamp indiquant l'absence de donnees depuis l'alarme
last_time_cmd_c2000 = time.time()

# Logfiles
if os.path.exists(config['LOG_MINIDO_FILE']): #and config['LOG_MINIDO']=='on':
    logfile = open(config['LOG_MINIDO_FILE'], 'a')
if os.path.exists(config['LOG_MUWATCH_FILE']): #and config['LOG_MUWATCH']=='on':
    logfile_mu_watch = open(config['LOG_MUWATCH_FILE'], 'a')
if os.path.exists(config['LOG_C2000_FILE']): #and config['LOG_C2000']=='on':
    logfile_c2000 = open(config['LOG_C2000_FILE'], 'a')

# Connect to Minido RS-485 Bus.
if( config['CONNTYPE_MINIDO'] == "Socket"):
    PySocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    print("{} {}".format(config['HOST_MINIDO'], int(config['PORT_MINIDO'])))
    PySocket.connect((config['HOST_MINIDO'], int(config['PORT_MINIDO'])))
    #PySocket.connect(('raspberry1', 2323))
    PySocket.settimeout(0.09)
    print_message(0, '', "Socket Minido connected\n")
else:
    ser = serial.Serial(
                    port=config['SERIAL_MINIDO'],
                    baudrate=19200,
                    parity=serial.PARITY_NONE,
                    stopbits=serial.STOPBITS_ONE,
                    bytesize=serial.EIGHTBITS,
                    timeout=1
                    )
    ser.open()
    if ser.isOpen():
        print_message(0, '', "Serial port Minido successfully opened.")

# Connect to C2000/Alarm RS-485 Bus.
if   (config['CONNTYPE_C2000'] == 'Socket'):
    Socket_C2000 = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    Socket_C2000.connect((config['HOST_C2000'], int(config['PORT_C2000'])))
    Socket_C2000.settimeout(0.01)
    print_message(0, '', "Socket C2000 connected\n")
elif (config['CONNTYPE_C2000'] == 'Serial'):
    ser2 = serial.Serial(
                    port=config['SERIAL_C2000'],
                    baudrate=19200,
                    parity=serial.PARITY_NONE,
                    stopbits=serial.STOPBITS_ONE,
                    bytesize=serial.EIGHTBITS,
                    timeout=1
                    )

    ser2.open()
    if ser2.isOpen():
        print_message(0, '', "Serial port C2000 successfully opened.\n")

# Connect to rfid bus
if (config['RFID'] == 'on'):
    ser_rfid = serial.Serial(
                    port=config['SERIAL_RFID'],
                    baudrate=9600,
                    parity=serial.PARITY_NONE,
                    stopbits=serial.STOPBITS_ONE,
                    bytesize=serial.EIGHTBITS,
                    timeout=1
                    )
    ser_rfid.open()
    if ser_rfid.isOpen():
        print_message(0, '', "Serial port rfid successfully opened.\n")
        
# Define socket
Socket_MU = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
Socket_MU.bind(('', int(config['PORT_MU'])))
Socket_MU.listen(10)
Socket_MU.setblocking(0)


# Main loop
while (Active):

    List_Key = list()

    if (config['CONNTYPE_MINIDO'] != "Socket"):
        time.sleep(0.1)
    
    retrieve_data_from_bus_minido()
    cmd = retrieve_command_from_buffer_minido()
    while (cmd!=''):
        update_Lists(cmd)
        cmd = retrieve_command_from_buffer_minido()
    
    
    retrieve_data_from_bus_c2000()
    cmd = retrieve_command_from_buffer_c2000()
    while (cmd!=''):
        process_command_from_buffer_c2000(cmd)
        cmd = retrieve_command_from_buffer_c2000()
            
    process_schedules()
    
    process_futur_actions_List()
    
    process_rfid()
    
    listen_socket_MU()
    
    heating_control()
    
    Conditions_evaluation()
    
    process_actions_trigger()
    
    update_DB()
    conn.commit()
    c2000_control()
    print_message(2, 'MU Debug', ' --- ' )
    
    if len(List_Key)>0:
        print_message(1, 'MU Debug', 'List_Key:' + str(List_Key) )


# Close
conn.close()
Socket_MU.close() 
if( config['CONNTYPE_MINIDO'] == 'Socket'):
    PySocket.close() 
else:
    ser.close()
if   (config['CONNTYPE_C2000'] == 'Socket'):
    Socket_C2000.close() 
elif (config['CONNTYPE_C2000'] == 'Serial'):
    ser2.close()
if (config['RFID'] == 'on'):
    ser_rfid.close()
