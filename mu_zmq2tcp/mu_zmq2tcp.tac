#!/usr/bin/python
# **- encoding: utf-8 -**
from twisted.application import internet, service
from twisted.application.service import Application
from twisted.internet import reactor

from txzmq import ZmqFactory, ZmqEndpoint, ZmqSubConnection, ZmqPushConnection

from mu_zmq2tcp import *

LISTEN_PORT = 2323

class ListenService(service.Service):
    def __init__(self):
        self.zf = ZmqFactory()
        self.minidoFactory = MinidoServerFactory()
        self.tcpService = internet.TCPServer(LISTEN_PORT, self.minidoFactory)

    def startService(self):
        self.sube = ZmqEndpoint('connect', 'tcp://localhost:5557')
        self.push = ZmqEndpoint('connect', 'tcp://localhost:5558')
        self.subconn = ZmqSubConnection(self.zf, self.sube)
        self.subconn.subscribe('')

        # Create the resource
        self.minidoFactory.pushconn = ZmqPushConnection(self.zf, self.push)
        self.subconn.gotMessage = self.minidoFactory.send2tcp
        self.tcpService.startService()

    def stopService(self):
        del self.sube
        del self.push
        del self.subconn

application = Application('ZMQ2TCP')
service = ListenService()
service.setServiceParent(application)
