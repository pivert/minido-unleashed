#!/usr/bin/python
# -*- coding: utf-8 -*-
# 
# References
# ==========
# http://www.web-calendar.org/fr/world/europe/belgium/arlon--wal?menu=sun
import ephem
from datetime import datetime
import sqlite3
import time
import argparse
from minido_ephem import DatetimeOfAzimuth

# Config
DB = "/opt/minido-unleashed/db/minidodb"
# Meix-le-Tige coordinates for ephem
LAT = 49.0 + 37.0/60 + 18.0/3600
LON = 5.0 + 43.0 / 60 + 9.0 / 3600

# Parse arguments
parser = argparse.ArgumentParser()
parser.add_argument("exo", help="exo number (between 1 and 16)", type=int)
parser.add_argument("output", help="output number on the selected exo", type=int, choices=range(1,9))
parser.add_argument("new_state", help="Action. If SHUTTERUP or SHUTTERDOWN is selected, it defaults to ON, and will issue an OFF after 30s. This is always the channel for UP command that must be provided.", choices=['ON', 'OFF', 'STATUS', 'SHUTTERUP', 'SHUTTERDOWN', 'SHUTTERSTOP'])
parser.add_argument("--sunaz", help="schedule a time in the next 24h when the sun will be at the specified position, currently accepted values are [sunrise|sunset|azimuth angle in °]. Default is now.")
parser.add_argument("--delay", help="add extra x seconds delay in the schedule")
parser.add_argument("--shutter-delay-off", help="Change the default delay of 30s to what's specified. Only used with shutters, typically used for a partial close.", type=int, default=30)
parser.add_argument("--verbose", help="verbose display mode", action="store_true")
parser.add_argument("--dry-run", help="Do not insert anything into the scheduler. Implies the --verbose", action="store_true")
args = parser.parse_args()

exo = format(args.exo + 0x3b, '02x')
# Functions
def adapt_datetime(ts):
    return time.mktime(ts.timetuple())

sqlite3.register_adapter(datetime, adapt_datetime)



gatech = ephem.Observer()
gatech.lat = str(LAT)
gatech.lon = str(LON)
gatech.date = datetime.utcnow()




if args.new_state == 'STATUS':
    query="SELECT last_status FROM minido_status WHERE module_addr=?;"
    p = (exo,)
    c.execute(query, p)
    result = c.fetchone()[0].split(' ')
    query="SELECT * FROM minido_devices WHERE module_addr=? AND idx=?"
    p = (exo,str(args.output - 1))
    c.execute(query, p)
    print('Status for ' + '|'.join([str(x) for x in c.fetchone()]) + ' :' + result[args.output - 1] if result else 'No Result')
    exit(0)

# New planet
sun = ephem.Sun()


## Calculate the timeshift from now, and make sure it's positive (not in the past).

# Take into account the optional delay from command line.
delay = args.delay if args.delay else 0.0
timeshift=float(delay) 

# Check options
if args.sunaz:
    if args.sunaz == "sunrise":
        tdr = ephem.localtime(gatech.next_rising(sun)) - datetime.now()
        timeshift += tdr.seconds
    elif args.sunaz == "sunset":
        tds = ephem.localtime(gatech.next_setting(sun)) - datetime.now()
        timeshift += tds.seconds
    elif args.sunaz.isdigit():
        doa = DatetimeOfAzimuth()
        tds = doa.get_localtime_from_azimuth(args.sunaz) - datetime.now()
        timeshift += tds.seconds
    else:
        print("The sun azimuth provided is not understood. Check with -h ")
        exit(1)

assert timeshift >= 0, "You cannot schedule in the past..."

# Verbose print
def verbose_print(timeshift, query, p):
        print("Action scheduled in {:02.2f} hours".format(timeshift/3600.0))
        print("Executing Query : " + str(query))
        print(" with parameters : " + str(p))

# DB operations
conn = sqlite3.connect(DB)
c = conn.cursor()
query="INSERT INTO exo_scheduler VALUES (?, ?, datetime('now', 'localtime', ?), NULL, ?, NULL)"

def db_write(exo, output, timeshift, ev):
    global args
    p = (exo, output - 1, '+' + str(int(timeshift)) + ' seconds', ev)
    if args.verbose:
        verbose_print(timeshift, query, p)

    if args.dry_run:
        print("Dry Run : Query not executed into the DB")
        verbose_print(timeshift, query, p)
    else:
        c.execute(query, p)

if args.new_state == 'SHUTTERDOWN':
    output = args.output
    for ev,offdelay in (('ON', 0), ('OFF',args.shutter_delay_off)):
        timeshift += offdelay
        db_write(exo, output, timeshift, ev)


elif args.new_state == 'SHUTTERUP':
    # Select the "other" output:
    output = (2 * (args.output // 2) - 1) if args.output % 2 == 0 else (2 * (args.output // 2) + 2)
    for ev,offdelay in (('ON', 0), ('OFF',args.shutter_delay_off)):
        timeshift += offdelay
        db_write(exo, output, timeshift, ev)

elif args.new_state == 'SHUTTERSTOP':
    output = args.output
    db_write(exo, output, timeshift, 'OFF')
    output = (2 * (args.output // 2) - 1) if args.output % 2 == 0 else (2 * (args.output // 2) + 2)
    db_write(exo, output, timeshift, 'OFF')

else:
    output = args.output
    p = (exo, args.output - 1, '+' + str(int(timeshift)) + ' seconds', args.new_state)
    db_write(exo, output, timeshift, ev)

conn.commit()
conn.close()

