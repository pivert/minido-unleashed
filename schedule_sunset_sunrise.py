#!/usr/bin/python
# 
# References
# ==========
# http://www.web-calendar.org/fr/world/europe/belgium/arlon--wal?menu=sun
import ephem
from datetime import datetime
import sqlite3
import time
import argparse

# Config
DB = "/opt/minido-unleashed/db/minidodb"
# Meix-le-Tige coordinates for ephem
LAT = 49.0 + 37.0/60 + 18.0/3600
LON = 5.0 + 43.0 / 60 + 9.0 / 3600

# Parse arguments
parser = argparse.ArgumentParser()
parser.add_argument("exo", help="exo number (between 1 and 16)", type=int)
parser.add_argument("output", help="output number on the selected exo", type=int, choices=range(1,9))
parser.add_argument("new_state", help="Action", choices=['ON', 'OFF', 'STATUS'])
parser.add_argument("--schedule", help="schedule a time in the next 24h, currently accepted values are [sunrise|sunset]. Default is now.", choices=['sunrise','sunset'])
parser.add_argument("--delay", help="add extra x seconds delay in the schedule")
parser.add_argument("--verbose", help="verbose display mode", action="store_true")
args = parser.parse_args()

delay = args.delay if args.delay else 0.0
exo = format(args.exo + 0x3b, '02x')
# Functions
def adapt_datetime(ts):
    return time.mktime(ts.timetuple())

sqlite3.register_adapter(datetime, adapt_datetime)



gatech = ephem.Observer()
gatech.lat = str(LAT)
gatech.lon = str(LON)
gatech.date = datetime.utcnow()


conn = sqlite3.connect(DB)
c = conn.cursor()


if args.new_state == 'STATUS':
    query="SELECT last_status FROM minido_status WHERE module_addr=?;"
    p = (exo,)
    c.execute(query, p)
    result = c.fetchone()[0].split(' ')
    query="SELECT * FROM minido_devices WHERE module_addr=? AND idx=?"
    p = (exo,str(args.output - 1))
    c.execute(query, p)
    print('Status for ' + '|'.join([str(x) for x in c.fetchone()]) + ' :' + result[args.output - 1] if result else 'No Result')
    exit(0)

# New planet
sun = ephem.Sun()

timeshift=float(delay)

if args.schedule:
    if args.schedule == "sunrise":
        tdr = ephem.localtime(gatech.next_rising(sun)) - datetime.now()
        timeshift += tdr.seconds
    elif args.schedule == "sunset":
        tds = ephem.localtime(gatech.next_setting(sun)) - datetime.now()
        timeshift += tds.seconds
    else:
        print("The schedule provided is not understood. Check with -h ")
        exit(1)

#print("Action scheduled in {:02.2f} hours".format(timeshift/3600.0))

query="INSERT INTO exo_scheduler VALUES (?, ?, datetime('now', 'localtime', ?), NULL, ?, NULL)"
p = (exo, args.output - 1, '+' + str(int(timeshift)) + ' seconds', args.new_state)
if args.verbose:
    print("Executing Query : " + str(query) + " with parameters : " + str(p))
c.execute(query, p)
conn.commit()
conn.close()

