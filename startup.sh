#!/bin/bash
LOG_TOPIC_OUT=$0
exec 3>&1 > >(tee -a /dev/fd/2 | logger -t "$LOG_TOPIC_OUT")
exec 2> >(logger -t "$LOG_TOPIC_OUT")

#echo "Changing to /opt/minido-unleashed-2.1/ folder"
#cd /opt/minido-unleashed-2.1/
#echo "Starting orbited"
#/usr/bin/screen -dmS mu /usr/local/bin/orbited -c ./etc/orbited.cfg
#sleep 1
#echo "Starting rs4852stomp"
#/usr/bin/screen -drS mu -X screen su -c "cd /opt/minido-unleashed-2.1/tools/ && twistd -noy rs485bus2stomp.tac" mu 
#sleep 1
#echo "Starting morbidq2tcp"
#/usr/bin/screen -drS mu -X screen su -c "cd /opt/minido-unleashed-2.1/tools/ && twistd -no --pidfile ./morbidq2tcp.pid -y morbidq2tcp.tac" mu
#sleep 1
echo "Starting mu_watch"
#/usr/bin/screen -dmS mu /usr/local/bin/orbited -c ./etc/orbited.cfg
/usr/bin/screen -dmS mu su -c "cd /opt/minido-unleashed/ && /usr/bin/python /opt/minido-unleashed/mu_watch.py" 
